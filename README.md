# google translate for emacs


Need to translate a bunch of resources from french to english ? Here's
a set of emacs macros for translating text using Google Translate.

## Installation

First, you need to install the [Emacs interface to Google Translate](https://github.com/atykhonov/google-translate):

Just run `M-x package-install RET google-translate RET`


Then, edit your `~/.emacs` and add the following lines:
```
(require 'google-translate)
(defun google-translate--search-tkk () "Search TKK." (list 430675 2721866130))
(setq google-translate-backend-method 'curl)

(fset 'translate-region-fr-to-en
   (kmacro-lambda-form [?\C-w ?\C-x ?o C-home ?\C-y ?\C-  C-S-end backspace ?\M-x ?g ?o ?o ?g tab ?b ?u tab return ?f ?r ?e tab return ?e ?n tab return ?\C-x ?o ?\C-s ?\[ ?l ?i ?s ?t ?e ?n end C-right C-left ?\C-  ?\C-s ?\C-s C-left left ?\M-w ?\C-x ?o ?\C-y ?\C-x ?\C-s] 0 "%d"))

```

## Translating a buffer

The [Emacs interface to Google
Translate](https://github.com/atykhonov/google-translate) provides a
macro for translating a buffer from one language to another. To use
it, run `M-x google-translate-buffer RET French RET English`

## Translating a region

The `translate-region-fr-to-en` macro (added to your `.emacs`)
translates a region of text. To use it, split your emacs windows in
two (`C-x 3`) and open a temporary file (`C-x o` and `C-x C-f
tempfile`). Then, select a region of text to translate and run
`M-x translate-region-fr-to-en`. The region of text will be replaced with
the translated text.
